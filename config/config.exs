use Mix.Config

# Configures the endpoint
config :cyric, CyricWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "dTBPiMbVtgGGL6bacgxaFekx4WD8ok8qpbzXqVIcUcvESQxvUs1u8wdpx4YU/rog",
  render_errors: [view: CyricWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Cyric.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "fzdsiQBg"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :tesla, adapter: Tesla.Adapter.Mint

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

if File.exists?("config/core.exs") do
  import_config "core.exs"
end
