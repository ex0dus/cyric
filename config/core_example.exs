use Mix.Config

config :cyric, :listeners, [
  %{
    type: "socks5",
    host: "localhost",
    port: 9000
  }
]

config :cyric, :proxies, [
  %{
    name: "ss1",
    type: "ss",
    host: "localhost",
    port: 8888,
    method: "chacha20-ietf-poly1305",
    password: "localhost"
  },
  %{
    name: "ssh1",
    type: "socks5",
    host: "localhost",
    port: 8889
  },
  %{
    name: "vmess1",
    type: "vmess",
    host: "localhost",
    port: 3333,
    uuid: "101aca33-f336-4f47-9dfc-934da46c610d"
  }
]

config :cyric, :rules, [
  "MATCH,vmess1"
]
