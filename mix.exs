defmodule Cyric.MixProject do
  use Mix.Project

  def project do
    [
      app: :cyric,
      version: "0.1.0",
      elixir: "~> 1.9",
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Cyric.Application, []},
      extra_applications: [:logger, :runtime_tools, :castore]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:hkdf, "~> 0.1.0"},
      {:fnv1a, "~> 0.1.0"},
      {:phoenix, "~> 1.4.14"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:plug_cowboy, "~> 2.0"},
      {:phoenix_live_view, "~> 0.8.0"},
      {:floki, ">= 0.0.0", only: :test},
      {:castore, "~> 0.1.0"},
      {:mint, "~> 1.0.0"},
      {:tesla, "~> 1.3.0"}
    ]
  end
end
