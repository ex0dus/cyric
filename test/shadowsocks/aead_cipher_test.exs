defmodule Cyric.Shadowsocks.CipherTest do
  use ExUnit.Case
  doctest Cyric.Shadowsocks.Cipher

  alias Cyric.Shadowsocks.AeadCipher

  test "encrypt and decrypt with chacha20-ietf-poly1305" do
    {:ok, pid} = AeadCipher.start_link(%{method: "chacha20-ietf-poly1305", password: "password"})
    data = Cyric.Shadowsocks.Cipher.encrypt_once(pid, "123")
    assert Cyric.Shadowsocks.Cipher.decrypt_once(pid, data) == "123"
  end

  test "encrypt and decrypt with aes-256-gcm" do
    {:ok, pid} = AeadCipher.start_link(%{method: "aes-256-gcm", password: "password"})
    data = Cyric.Shadowsocks.Cipher.encrypt_once(pid, "123")
    assert Cyric.Shadowsocks.Cipher.decrypt_once(pid, data) == "123"
  end

  test "encrypt and decrypt with aes-192-gcm" do
    {:ok, pid} = AeadCipher.start_link(%{method: "aes-192-gcm", password: "password"})
    data = Cyric.Shadowsocks.Cipher.encrypt_once(pid, "123")
    assert Cyric.Shadowsocks.Cipher.decrypt_once(pid, data) == "123"
  end

  test "encrypt and decrypt with aes-128-gcm" do
    {:ok, pid} = AeadCipher.start_link(%{method: "aes-128-gcm", password: "password"})
    data = Cyric.Shadowsocks.Cipher.encrypt_once(pid, "123")
    assert Cyric.Shadowsocks.Cipher.decrypt_once(pid, data) == "123"
  end
end
