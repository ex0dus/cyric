defmodule Mix.Tasks.Helper do
  defmodule FetchCnDomains do
    use Mix.Task

    def run(_) do
      Mix.shell().info("fetching china domain list from 'felixonmars/dnsmasq-china-list'...")

      {:ok, resp} =
        Tesla.get(
          "https://raw.githubusercontent.com/felixonmars/dnsmasq-china-list/master/accelerated-domains.china.conf"
        )

      data =
        resp.body
        |> String.split("\n")
        |> Stream.map(&String.split(&1, "/"))
        |> Stream.map(&Enum.at(&1, 1))
        |> Stream.reject(&is_nil/1)
        |> Enum.join("\n")

      :ok = File.write("priv/rules/cn_domains.txt", data)
      Mix.shell().info("saved china domain list to 'priv/rules/cn_domains.txt'")
    end
  end
end
