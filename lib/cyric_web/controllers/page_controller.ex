defmodule CyricWeb.PageController do
  use CyricWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
