defmodule CyricWeb.Router do
  use CyricWeb, :router

  import Phoenix.LiveView.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", CyricWeb do
    pipe_through(:browser)

    get "/", Redirect, to: "/stats"
    live "/stats", StatsLive
    live "/proxies", ProxiesLive
    live "/rules", RulesLive
  end

  scope "/api", CyricWeb do
    pipe_through(:api)
  end
end

defmodule CyricWeb.Redirect do
  use CyricWeb, :router

  def init(opts), do: opts

  def call(conn, opts) do
    conn
    |> Phoenix.Controller.redirect(opts)
    |> halt()
  end
end
