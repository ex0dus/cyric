defmodule CyricWeb.PageView do
  use CyricWeb, :view

  def display_size(size, level \\ 0) do
    if size < 1024 || level == 3 do
      "#{(size * 1.0) |> Float.floor(2)} #{suffix(level)}"
    else
      display_size(size / 1024, level + 1)
    end
  end

  defp suffix(level) do
    case level do
      0 -> "b"
      1 -> "k"
      2 -> "M"
      3 -> "G"
    end
  end
end
