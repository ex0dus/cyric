defmodule CyricWeb.RulesLive do
  use Phoenix.LiveView

  alias CyricWeb.PageView

  def render(assigns) do
    PageView.render("rules.html", assigns)
  end

  def mount(_params, _session, socket) do
    {:ok, update_rules(socket)}
  end

  defp update_rules(socket) do
    rules_map =
      Cyric.Dispatcher.fetch_rules()
      |> Enum.flat_map(fn {type, rule_group} ->
        case rule_group do
          proxy when is_bitstring(proxy) ->
            [%{type: type, value: "N/A", proxy: proxy}]

          _ ->
            Enum.map(rule_group, fn {value, proxy} ->
              %{type: type, value: value, proxy: proxy}
            end)
        end
      end)

    assign(socket, rules: rules_map)
  end
end
