defmodule CyricWeb.StatsLive do
  use Phoenix.LiveView

  alias CyricWeb.PageView

  def render(assigns) do
    PageView.render("stats.html", assigns)
  end

  def mount(_params, _session, socket) do
    if connected?(socket), do: :timer.send_interval(1000, self(), :tick)
    {:ok, update_stats(socket)}
  end

  def handle_info(:tick, socket) do
    {:noreply, update_stats(socket)}
  end

  defp update_stats(socket) do
    assign(socket, stats: Cyric.StatsHub.fetch_stats())
  end
end
