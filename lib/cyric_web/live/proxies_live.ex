defmodule CyricWeb.ProxiesLive do
  use Phoenix.LiveView

  alias CyricWeb.PageView

  def render(assigns) do
    PageView.render("proxies.html", assigns)
  end

  def mount(_params, _session, socket) do
    {:ok, update_proxies(socket)}
  end

  defp update_proxies(socket) do
    assign(socket, proxies: Cyric.Dispatcher.fetch_proxies())
  end
end
