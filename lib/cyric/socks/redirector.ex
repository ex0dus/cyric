defmodule Cyric.Socks.Redirector do
  use Cyric.Redirector

  alias Cyric.Address
  alias Cyric.Socks.Protocol

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def handle_init(conf) do
    %{host: host, port: port} = conf

    {:ok, tcp_stream} =
      Cyric.Tcp.StreamSup.start_stream({:outgoing, Address.new(:domain, host, port), false})

    stream_out = %Cyric.RawStream{tcp_stream: tcp_stream}
    {:ok, %State{out: stream_out}}
  end

  @impl true
  def handle_connect(address, %State{out: stream} = state) do
    with :ok <- Streamable.write(stream, <<5, 1, 0>>),
         {:ok, <<5, 0>>} <- Streamable.read(stream, 2),
         :ok <- Streamable.write(stream, <<5, 1, 0>>),
         :ok <- Protocol.write_address(stream, address),
         {:ok, <<5, 0, 0>>} <- Streamable.read(stream, 3),
         {:ok, _} <- Protocol.read_address(stream) do
      {:ok, state}
    else
      reason ->
        {:error, reason}
    end
  end
end
