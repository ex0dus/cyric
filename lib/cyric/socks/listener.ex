defmodule Cyric.Socks.Listener do
  use GenServer
  require Logger

  alias Cyric.Socks.Protocol
  alias Cyric.{Streamable, Address}

  @unspecified_address Address.new(:ipv4, {0, 0, 0, 0}, 0)
  @socket_opts [:binary, packet: :raw, active: false, reuseaddr: true]

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def init(config) do
    Logger.debug("starting socks5 listener")
    %{host: host, port: port} = config
    {:ok, ip} = :inet.getaddr(to_charlist(host), :inet)

    {:ok, socket} = :gen_tcp.listen(port, [{:ip, ip} | @socket_opts])

    {:ok, socket, {:continue, :accept}}
  end

  def handle_continue(:accept, socket) do
    {:ok, client} = :gen_tcp.accept(socket)

    {:ok, task} = Task.start(fn -> dispatch(client) end)
    :gen_tcp.controlling_process(client, task)

    {:noreply, socket, {:continue, :accept}}
  end

  def dispatch(socket) do
    {:ok, tcp_stream} = Cyric.Tcp.StreamSup.start_stream({:incoming, socket, false})
    # {:ok, tcp_stream} = Cyric.Tcp.Stream.start({:incoming, socket, false})
    :ok = :gen_tcp.controlling_process(socket, tcp_stream)

    stream = %Cyric.RawStream{tcp_stream: tcp_stream}

    with {:ok, <<5, nmethods>>} <- Streamable.read(stream, 2),
         {:ok, <<_::binary-size(nmethods)>>} <- Streamable.read(stream, nmethods),
         :ok <- Streamable.write(stream, <<5, 0>>),
         {:ok, <<5, 1, 0>>} <- Streamable.read(stream, 3),
         {:ok, target} <- Protocol.read_address(stream),
         :ok <- Streamable.write(stream, <<5, 0, 0>>),
         :ok <- Protocol.write_address(stream, @unspecified_address) do
      Cyric.Dispatcher.redirect({target, stream})
      {:ok, target}
    else
      reason ->
        # try to close socket
        Streamable.write(stream, <<5, 1, 0>>)
        Logger.debug("failed to accept socks connection #{inspect(reason)}")
        {:error, reason}
    end
  end
end
