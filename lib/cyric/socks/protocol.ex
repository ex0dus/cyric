defmodule Cyric.Socks.Protocol do
  @atyp_ipv4 <<1>>
  @atyp_ipv6 <<4>>
  @atyp_domain <<3>>

  alias Cyric.{Streamable, Address}

  def read_address(stream) do
    case Streamable.read(stream, 1) do
      {:ok, @atyp_ipv4} ->
        {:ok, <<a, b, c, d, port::16>>} = Streamable.read(stream, 6)
        {:ok, Address.new(:ipv4, {a, b, c, d}, port)}

      {:ok, @atyp_domain} ->
        with {:ok, <<len>>} <- Streamable.read(stream, 1) do
          {:ok, <<domain::binary-size(len), port::16>>} = Streamable.read(stream, len + 2)
          {:ok, Address.new(:domain, domain, port)}
        end

      {:ok, @atyp_ipv6} ->
        {:ok, <<ip::binary-size(16), port::16>>} = Streamable.read(stream, 18)
        {:ok, Address.new(:ipv6, ip, port)}

      _ ->
        {:error, "addr not supported"}
    end
  end

  def write_address(stream, address) do
    Streamable.write(stream, Address.to_socks_binary(address))
  end
end
