defmodule Cyric.RedirectorSup do
  use DynamicSupervisor

  require Logger

  def start_link(args) do
    DynamicSupervisor.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl true
  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_redirect({redmod, conf}, target, stream_in) do
    with {:ok, pid} <- DynamicSupervisor.start_child(__MODULE__, {redmod, {stream_in, conf}}),
         :ok <- Cyric.Redirector.connect(pid, target) do
      Cyric.Redirector.start_pipe(pid)
    else
      reason ->
        Logger.debug("error redirecting #{inspect(reason)}")
        Cyric.Tcp.Stream.close(stream_in)
    end
  end
end
