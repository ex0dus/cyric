defmodule Cyric.Rule.Matcher.DomainKeyword do
  def match(rule_set, domain) do
    rule_set
    |> Map.get(:domain_keyword, %{})
    |> Enum.find_value(fn {keyword, proxy} ->
      String.contains?(domain, keyword) && proxy
    end)
  end
end
