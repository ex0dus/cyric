defmodule Cyric.Rule.Matcher.DomainSuffix do
  # TODO: generate suffix then fetch from map?
  def match(rule_set, domain) do
    rule_set
    |> Map.get(:domain_suffix, [])
    |> Enum.find_value(fn {suffix, proxy} ->
      String.ends_with?(domain, suffix) && proxy
    end)
  end
end
