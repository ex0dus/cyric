defmodule Cyric.Rule.Matcher.Domain do
  def match(rule_set, domain) do
    rule_set
    |> Map.get(:domain, %{})
    |> Map.get(domain)
  end
end
