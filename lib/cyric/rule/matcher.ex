defmodule Cyric.Rule.Matcher do
  use GenServer

  alias Cyric.Address
  alias Cyric.Rule.Matcher.{Domain, DomainSuffix, DomainKeyword}

  require Logger

  @domain_matchers [Domain, DomainSuffix, DomainKeyword]
  # TODO:
  # GEOIP
  # USER-AGENT
  # IP-CIDR
  # SRC-IP-CIDR
  # DST-PORT
  # SRC-PORT

  @impl true
  def init(_args) do
    {:ok, nil}
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def match(rule_set, %Address{type: type, host: host}) do
    case type do
      :domain ->
        match_domains(rule_set, [host])

      :ipv4 ->
        # TODO: match ip based rules
        case Cyric.Dns.Resolver.reverse_lookup(host) do
          nil ->
            :not_found

          domains ->
            Logger.info("reverse lookup domains: #{inspect(domains)}")
            match_domains(rule_set, domains)
        end

      _ ->
        :not_found
    end
  end

  defp match_domains(rules, domains) do
    Enum.flat_map(domains, fn domain ->
      Enum.map(@domain_matchers, fn matcher ->
        {matcher, domain}
      end)
    end)
    |> Task.async_stream(fn {matcher, domain} -> matcher.match(rules, domain) end, ordered: false)
    |> Enum.find(fn result -> !match?({:ok, nil}, result) end)
  end
end
