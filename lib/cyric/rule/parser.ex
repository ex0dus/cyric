defmodule Cyric.Rule.Parser do
  require Logger

  def parse_proxies(config) do
    config
    |> Stream.map(fn %{name: name} = conf ->
      case conf do
        %{type: "socks5"} ->
          {name, {Cyric.Socks.Redirector, conf}}

        %{type: "ss"} ->
          {name, {Cyric.Shadowsocks.Redirector, conf}}

        %{type: "vmess"} ->
          {name, {Cyric.Vmess.Redirector, conf}}

        _ ->
          # FIXME: ignore for now
          nil
      end
    end)
    |> Enum.reject(&is_nil/1)
    |> Enum.into(%{})
  end

  def parse_rules(config) do
    config
    |> Stream.map(&String.split(&1, ","))
    |> Enum.reduce(%{}, fn rule, rule_set ->
      case rule do
        ["DOMAIN-SUFFIX", suffix, proxy] ->
          Map.update(rule_set, :domain_suffix, %{suffix => proxy}, fn m ->
            Map.put(m, suffix, proxy)
          end)

        ["DOMAIN", domain, proxy] ->
          Map.update(rule_set, :domain, %{domain => proxy}, fn m ->
            Map.put(m, domain, proxy)
          end)

        ["DOMAIN-KEYWORD", keyword, proxy] ->
          Map.update(rule_set, :domain_keyword, %{keyword => proxy}, fn m ->
            Map.put(m, keyword, proxy)
          end)

        ["DOMAINS-CN", proxy] ->
          domains_cn = gen_domains_cn_rules(proxy)

          Map.update(rule_set, :domain_suffix, domains_cn, fn m ->
            Map.merge(m, domains_cn)
          end)

        ["MATCH", proxy] ->
          Map.put(rule_set, :match, proxy)

        _ ->
          # FIXME: ignoring other rules
          rule_set
      end
    end)
  end

  @cn_domains_file "priv/rules/cn_domains.txt"
  defp gen_domains_cn_rules(proxy) do
    if File.exists?(@cn_domains_file) do
      File.stream!("priv/rules/cn_domains.txt")
      |> Stream.map(&String.trim/1)
      |> Stream.map(fn suffix ->
        {suffix, proxy}
      end)
      |> Enum.into(%{})
    else
      Logger.info(
        "no cn_domains file found, please generate it with `mix helper.fetch_cn_domains`"
      )

      %{}
    end
  end
end
