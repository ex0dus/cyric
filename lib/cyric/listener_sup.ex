defmodule Cyric.ListenerSup do
  use Supervisor

  def start_link(config) do
    Supervisor.start_link(__MODULE__, config, name: __MODULE__)
  end

  def init(listeners) do
    # TODO: do a config check
    children =
      listeners
      |> Enum.map(fn %{type: type} = config ->
        case type do
          "socks5" -> {Cyric.Socks.Listener, config}
          "dns" -> {Cyric.Dns.Listener, config}
          "tcp" -> {Cyric.Tcp.Listener, config}
          _ -> nil
        end
      end)

    Supervisor.init(children, strategy: :one_for_one)
  end
end
