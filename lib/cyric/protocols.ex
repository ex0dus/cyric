defprotocol Cyric.Streamable do
  def read(stream, len \\ 0)
  def write(stream, data)
  def close(stream)
  def set_active(stream, active)
end

defprotocol Cyric.Codec do
  def encode(codec, raw)
  def decode(codec, encoded)
end
