defmodule Cyric.RawStream do
  defstruct tcp_stream: nil
end

defimpl Cyric.Streamable, for: Cyric.RawStream do
  alias Cyric.RawStream
  alias Cyric.Tcp.Stream

  def read(%RawStream{tcp_stream: stream}, len) do
    Stream.read(stream, len)
  end

  def write(%RawStream{tcp_stream: stream}, payload) do
    Stream.write(stream, payload)
  end

  def set_active(%RawStream{tcp_stream: stream}, active) do
    Stream.set_active(stream, active)
  end

  def close(%RawStream{tcp_stream: stream}) do
    Stream.close(stream)
  end
end
