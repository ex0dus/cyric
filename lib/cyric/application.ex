defmodule Cyric.Application do
  use Application

  def start(_type, _args) do
    children = [
      CyricWeb.Endpoint,
      Cyric.StatsHub,
      Cyric.Tcp.StreamSup,
      Cyric.RedirectorSup,
      {Cyric.ListenerSup, Application.fetch_env!(:cyric, :listeners)},
      {Cyric.Dns.Resolver, Application.fetch_env!(:cyric, :dns)},
      {Cyric.Dispatcher,
       [Application.fetch_env!(:cyric, :proxies), Application.fetch_env!(:cyric, :rules)]}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: __MODULE__)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CyricWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
