defmodule Cyric.Shadowsocks.AeadCipher do
  use GenServer, restart: :temporary

  import Bitwise

  @aad <<>>
  @aead_nonce_len 12
  @max_payload_len 16383
  @min_payload_len 18
  @ciphers %{
    "chacha20-ietf-poly1305" => {:chacha20_poly1305, 32, 32},
    "aes-128-gcm" => {:aes_128_gcm, 16, 16},
    "aes-192-gcm" => {:aes_192_gcm, 24, 24},
    "aes-256-gcm" => {:aes_256_gcm, 32, 32}
  }

  defmodule State do
    defstruct cipher_t: nil, pre_key: nil, e_ctx: nil, d_ctx: nil

    @type t :: %State{
            cipher_t: {atom, non_neg_integer, non_neg_integer},
            pre_key: binary,
            e_ctx: {binary, binary, non_neg_integer, binary},
            d_ctx: {binary, binary, non_neg_integer, binary}
          }
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def init(%{method: method, password: pass}) do
    {_cipher, klen, _salt} = cipher_t = Map.get(@ciphers, method)
    pre_key = gen_pre_key(klen, pass)

    {:ok, %State{cipher_t: cipher_t, pre_key: pre_key}}
  end

  @impl true
  def handle_call({:encrypt_once, payload}, _from, %State{} = state) do
    %{cipher_t: {cipher, _klen, slen}, pre_key: pre_key} = state
    # always generate key salt when encrypt once
    salt = :crypto.strong_rand_bytes(slen)
    key = aead_subkey(pre_key, salt)
    {:ok, data, _new_nonce} = encrypt(cipher, key, 0, payload)
    {:reply, salt <> data, state}
  end

  @impl true
  def handle_call({:decrypt_once, payload}, _from, %State{} = state) do
    %{cipher_t: {cipher, _klen, slen}, pre_key: pre_key} = state
    # always generate key salt when decrypt once
    <<salt::binary-size(slen), remains::binary>> = payload
    key = aead_subkey(pre_key, salt)
    {:ok, data, _new_nonce, _leftover} = decrypt(cipher, key, 0, remains, <<>>)
    {:reply, data, state}
  end

  @impl true
  def handle_call({:encrypt, <<>>}, _from, %State{} = state) do
    {:reply, <<>>, state}
  end

  @impl true
  def handle_call({:encrypt, payload}, _from, %State{} = state) do
    %{cipher_t: {cipher, _klen, slen}, e_ctx: ctx} = state

    case ctx do
      {key, salt, nonce} ->
        {:ok, data, new_nonce} = encrypt(cipher, key, nonce, payload)
        {:reply, data, %State{state | e_ctx: {key, salt, new_nonce}}}

      _ ->
        %{pre_key: pre_key} = state
        salt = :crypto.strong_rand_bytes(slen)
        key = aead_subkey(pre_key, salt)
        {:ok, data, new_nonce} = encrypt(cipher, key, 0, payload)
        {:reply, salt <> data, %State{state | e_ctx: {key, salt, new_nonce}}}
    end
  end

  @impl true
  def handle_call({:decrypt, <<>>}, _from, %State{} = state) do
    {:reply, <<>>, state}
  end

  @impl true
  def handle_call({:decrypt, payload}, _from, %State{} = state) do
    %{cipher_t: {cipher, _klen, slen}, d_ctx: ctx} = state

    case ctx do
      {key, salt, nonce, leftover} ->
        {:ok, data, new_nonce, leftover} = decrypt(cipher, key, nonce, leftover <> payload, <<>>)
        {:reply, data, %State{state | d_ctx: {key, salt, new_nonce, leftover}}}

      _ ->
        %{pre_key: pre_key} = state
        <<salt::binary-size(slen), remains::binary>> = payload
        key = aead_subkey(pre_key, salt)
        {:ok, data, new_nonce, leftover} = decrypt(cipher, key, 0, remains, <<>>)
        {:reply, data, %State{state | d_ctx: {key, salt, new_nonce, leftover}}}
    end
  end

  defp decrypt(_cipher, _key, nonce, <<>>, acc), do: {:ok, acc, nonce, <<>>}

  defp decrypt(_cipher, _key, nonce, payload, acc) when byte_size(payload) < @min_payload_len,
    do: {:ok, acc, nonce, payload}

  defp decrypt(cipher, key, nonce, payload, acc) do
    <<len_enc::binary-size(2), len_tag::binary-size(16), remain::binary>> = payload

    <<len::size(16)>> =
      :crypto.crypto_one_time_aead(cipher, key, aead_nonce(nonce), len_enc, @aad, len_tag, false)

    if len + 16 > byte_size(remain) do
      {:ok, acc, nonce, payload}
    else
      <<data_enc::binary-size(len), data_tag::binary-size(16), next::binary>> = remain

      data =
        :crypto.crypto_one_time_aead(
          cipher,
          key,
          aead_nonce(nonce + 1),
          data_enc,
          @aad,
          data_tag,
          false
        )

      decrypt(cipher, key, nonce + 2, next, acc <> data)
    end
  end

  defp encrypt(cipher, key, nonce, payload) do
    len = IO.iodata_length(payload)

    if len > @max_payload_len do
      len_bin = <<0::2, @max_payload_len::14>>

      {len_enc, len_tag} =
        :crypto.crypto_one_time_aead(cipher, key, aead_nonce(nonce), len_bin, @aad, true)

      <<data::binary-size(@max_payload_len), leftover::binary>> = payload

      {payload_enc, payload_tag} =
        :crypto.crypto_one_time_aead(cipher, key, aead_nonce(nonce + 1), data, @aad, true)

      enc_data = len_enc <> len_tag <> payload_enc <> payload_tag
      {:ok, lo_data, new_nonce} = encrypt(cipher, key, nonce + 2, leftover)

      {:ok, enc_data <> lo_data, new_nonce}
    else
      len_bin = <<0::2, len::14>>

      {len_enc, len_tag} =
        :crypto.crypto_one_time_aead(cipher, key, aead_nonce(nonce), len_bin, @aad, true)

      {payload_enc, payload_tag} =
        :crypto.crypto_one_time_aead(cipher, key, aead_nonce(nonce + 1), payload, @aad, true)

      {:ok, len_enc <> len_tag <> payload_enc <> payload_tag, nonce + 2}
    end
  end

  defp aead_nonce(ctr) when ctr < 1 <<< (@aead_nonce_len * 8) do
    len = @aead_nonce_len * 8
    <<ctr::little-size(len)>>
  end

  defp aead_subkey(pre_key, salt) do
    HKDF.expand(:sha, HKDF.extract(:sha, pre_key, salt), 32, <<"ss-subkey">>)
  end

  def gen_pre_key(keylen, pass) do
    gen_key(pass, keylen, <<>>)
  end

  defp gen_key(_, keylen, acc) when byte_size(acc) >= keylen do
    <<key::binary-size(keylen), _::binary>> = acc
    key
  end

  defp gen_key(pass, keylen, acc) do
    digest = :crypto.hash(:md5, <<acc::binary, pass::binary>>)
    gen_key(pass, keylen, <<acc::binary, digest::binary>>)
  end
end
