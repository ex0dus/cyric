defmodule Cyric.Shadowsocks.Redirector do
  use Cyric.Redirector

  alias Cyric.Address
  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def handle_init(conf) do
    %{host: host, port: port} = conf

    {:ok, ts} =
      Cyric.Tcp.StreamSup.start_stream({:outgoing, Address.new(:domain, host, port), false})

    codec = Cyric.Shadowsocks.Codec.new(conf)
    stream_out = Cyric.EncodedStream.new(ts, codec)
    {:ok, %State{out: stream_out}}
  end

  @impl true
  def handle_connect(%Address{} = address, %State{out: stream} = state) do
    with :ok <- Streamable.write(stream, Address.to_socks_binary(address)) do
      {:ok, state}
    else
      reason ->
        Logger.debug("shadowsocks stream connect failed #{inspect(reason)}")
        {:error, reason}
    end
  end
end
