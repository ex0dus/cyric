defmodule Cyric.Shadowsocks.Codec do
  defstruct cipher: nil

  alias Cyric.Shadowsocks.Codec

  def new(conf) do
    {:ok, cipher} = Cyric.Shadowsocks.AeadCipher.start_link(conf)
    %Codec{cipher: cipher}
  end

  def encode(%Codec{cipher: cipher}, raw) do
    GenServer.call(cipher, {:encrypt, raw})
  end

  def decode(%Codec{cipher: cipher}, encoded) do
    GenServer.call(cipher, {:decrypt, encoded})
  end
end

defimpl Cyric.Codec, for: Cyric.Shadowsocks.Codec do
  alias Cyric.Shadowsocks.Codec

  def encode(codec, raw) do
    Codec.encode(codec, raw)
  end

  def decode(codec, encoded) do
    Codec.decode(codec, encoded)
  end
end
