defmodule Cyric.Address do
  @typedoc """
  a struct present a valid ipv4/domain/ipv6 address
  """
  @type t() :: %__MODULE__{}

  @atyp_ipv4 <<1>>
  @atyp_ipv6 <<4>>
  @atyp_domain <<3>>

  alias Cyric.Address

  defstruct type: :ipv4, host: {0, 0, 0, 0}, port: 0

  @spec new(type :: :ipv4 | :ipv6 | :domain, host :: binary | tuple, port :: integer) :: t()
  def new(type, host, port) do
    %Address{type: type, host: host, port: port}
  end

  def to_inet(%Address{type: type, host: host, port: port}) do
    case type do
      :ipv4 ->
        {:ok, host, port}

      :domain ->
        {:ok, addr} = :inet.getaddr(to_charlist(host), :inet)
        {:ok, addr, port}
    end
  end

  def to_socks_binary(%Address{type: type, host: host, port: port}) do
    case type do
      :ipv4 ->
        @atyp_ipv4 <> binary_ipv4(host) <> <<port::16>>

      :ipv6 ->
        @atyp_ipv6 <> host <> <<port::16>>

      :domain ->
        size = String.length(host)
        @atyp_domain <> <<size>> <> host <> <<port::16>>
    end
  end

  def to_vmess_binary(%Address{type: type, host: host, port: port}) do
    case type do
      :ipv4 ->
        <<port::16>> <> <<1>> <> binary_ipv4(host)

      :domain ->
        size = byte_size(host)
        <<port::16>> <> <<2>> <> <<size>> <> host

      :ipv6 ->
        {:error, "ipv6 is not supported"}
    end
  end

  defp binary_ipv4(ipv4_address) do
    {a, b, c, d} = ipv4_address
    <<a, b, c, d>>
  end
end
