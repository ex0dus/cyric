defmodule Cyric.Redirector do
  @callback handle_connect(address :: Cyric.Address.t(), state :: Cyric.Redirector.State.t()) ::
              {:ok, state :: Cyric.Redirector.State.t()} | {:error, reason :: String.t()}
  @callback handle_init(conf :: map) :: {:ok, state :: map}

  def connect(pid, address) do
    GenServer.call(pid, {:connect, address})
  end

  def start_pipe(pid) do
    GenServer.cast(pid, :start_pipe)
  end

  defmacro __using__(_opts) do
    quote do
      @behaviour Cyric.Redirector

      require Logger

      defmodule State do
        @type t() :: %__MODULE__{}
        defstruct name: "", in: nil, out: nil, conf: nil
      end

      use GenServer, restart: :temporary

      alias Cyric.Streamable
      alias Cyric.Socks.Protocol

      @impl true
      def init({stream_in, %{name: name} = conf}) do
        {:ok, state} = handle_init(conf)
        Cyric.StatsHub.track_connection({:up, name})
        {:ok, %{state | name: name, in: stream_in, conf: conf}}
      end

      @impl true
      def terminate(_reason, %{name: name} = state) do
        Cyric.StatsHub.track_connection({:down, name})
      end

      @impl true
      def handle_call({:connect, address}, _caller, %State{name: name, out: stream} = state) do
        case handle_connect(address, state) do
          {:ok, state} ->
            {:reply, :ok, state}

          {:error, reason} ->
            {:stop, {:shutdown, reason}, state}
        end
      end

      @impl true
      def handle_cast(:start_pipe, %State{} = state) do
        %{in: stream_in, out: stream_out} = state

        with :ok <- Streamable.set_active(stream_in, true),
             :ok <- Streamable.set_active(stream_out, true) do
          Process.send_after(self(), :pipe, 100)
        end

        {:noreply, state}
      end

      @impl true
      def handle_info(:pipe, %State{} = state) do
        %{in: src, out: dst, conf: %{name: name}} = state

        with {:ok, src_data} <- Streamable.read(src),
             :ok <- Streamable.write(dst, src_data),
             {:ok, dst_data} <- Streamable.read(dst),
             :ok <- Streamable.write(src, dst_data) do
          Cyric.StatsHub.track_transfer({:up, name, byte_size(src_data)})
          Cyric.StatsHub.track_transfer({:down, name, byte_size(dst_data)})
          Process.send_after(self(), :pipe, 100)
          {:noreply, state}
        else
          reason ->
            Streamable.close(src)
            Streamable.close(dst)
            Logger.info("redirector shutdown: #{inspect(reason)}")
            {:stop, {:shutdown, reason}, state}
        end
      end
    end
  end
end
