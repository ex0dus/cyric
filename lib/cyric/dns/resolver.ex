defmodule Cyric.Dns.Resolver do
  use GenServer

  @opts [:binary, active: true]

  defmodule State do
    defstruct queries: %{}, mapping: %{}, socket: nil, server: nil, port: 53
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def resolve({socket, address, port, data}) do
    GenServer.cast(__MODULE__, {:resolve, socket, address, port, data})
  end

  def reverse_lookup(host) do
    GenServer.call(__MODULE__, {:lookup, host})
  end

  @impl true
  def init(%{server: server, port: port}) do
    {:ok, socket} = :gen_udp.open(0, @opts)
    {:ok, %State{socket: socket, server: server, port: port}}
  end

  @impl true
  def handle_call({:lookup, host}, _caller, %State{mapping: mapping} = state) do
    {:reply, Map.get(mapping, host), state}
  end

  @impl true
  def handle_cast(
        {:resolve, socket, address, port, data},
        %State{server: server, port: server_port} = state
      ) do
    %{socket: s_socket, queries: queries} = state

    # send dns query to upstream dns server
    :gen_udp.send(s_socket, server, server_port, data)

    # save query for response
    message = Cyric.Dns.Message.from(data)

    {:noreply, %{state | queries: Map.put(queries, message.query_id, {socket, address, port})}}
  end

  @impl true
  # handle resolve result
  def handle_info({:udp, _socket, _address, _port, data}, %State{} = state) do
    message = Cyric.Dns.Message.from(data)

    # update host-domain mapping
    new_state =
      Map.update(state, :mapping, %{}, fn mapping ->
        Enum.reduce(message.records, mapping, fn {domain, host}, mapping ->
          Map.update(mapping, host, MapSet.new([domain]), fn domains ->
            MapSet.put(domains, domain)
          end)
        end)
      end)

    {:noreply, new_state, {:continue, {:response, message.query_id, data}}}
  end

  @impl true
  def handle_continue({:response, query_id, data}, %State{queries: queries} = state) do
    # handle response
    case Map.get(queries, query_id) do
      {socket, address, port} ->
        :gen_udp.send(socket, address, port, data)
        {:noreply, state |> Map.put(:queries, Map.delete(queries, query_id))}

      _ ->
        {:noreply, state}
    end
  end
end
