defmodule Cyric.Dns.Listener do
  use GenServer

  require Logger

  @opts [:binary, active: true]

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def init(%{port: port}) do
    :gen_udp.open(port, @opts)
  end

  @impl true
  def handle_info({:udp, socket, address, port, data}, state) do
    Cyric.Dns.Resolver.resolve({socket, address, port, data})
    {:noreply, state}
  end
end
