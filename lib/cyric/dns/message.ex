defmodule Cyric.Dns.Message do
  defstruct raw: nil, head: nil, qdcount: 0, ancount: 0, query_id: nil, questions: [], records: []

  @type_A 1
  @class_IN 1

  alias Cyric.Dns.Message

  def from(packet) do
    %Message{raw: packet, head: packet}
    |> parse_header()
    |> parse_questions()
    |> parse_records()
  end

  defp parse_header(%Message{head: head} = message) do
    <<query_id::16, _::16, qdcount::16, ancount::16, _::32, head::binary>> = head
    %Message{message | query_id: query_id, qdcount: qdcount, ancount: ancount, head: head}
  end

  defp parse_questions(%Message{qdcount: count} = message) when count == 0, do: message

  defp parse_questions(%Message{qdcount: count, head: head, questions: questions} = message) do
    {:ok, name, head} = parse_name(head, message.raw)
    <<qtype::16, qclass::16, head::binary>> = head

    parse_questions(%Message{
      message
      | questions: [{name, qtype, qclass} | questions],
        qdcount: count - 1,
        head: head
    })
  end

  defp parse_records(%Message{ancount: count} = message) when count == 0, do: message

  defp parse_records(%Message{ancount: count, head: head, records: records} = message) do
    {:ok, name, head} = parse_name(head, message.raw)
    <<type::16, class::16, _ttl::32, rlen::16, head::binary>> = head

    case {type, class, rlen} do
      {@type_A, @class_IN, 4} ->
        # rlen should be 4
        <<a, b, c, d, head::binary>> = head

        parse_records(%Message{
          message
          | records: [{name, {a, b, c, d}} | records],
            ancount: count - 1,
            head: head
        })

      _ ->
        # we only cares about A record drop other records
        <<_::binary-size(rlen), head::binary>> = head
        parse_records(%Message{message | head: head, ancount: count - 1})
    end
  end

  defp parse_name(data, raw, acc \\ <<>>)

  # in response, name might be compressed
  defp parse_name(<<1::1, 1::1, offset::14, data::binary>>, raw, acc) do
    <<_::binary-size(offset), name_data::binary>> = raw
    {:ok, name, _data} = parse_name(name_data, raw, acc)
    {:ok, name, data}
  end

  defp parse_name(<<len::8, data::binary>>, _raw, acc) when len == 0, do: {:ok, acc, data}

  defp parse_name(<<len::8, data::binary>>, raw, <<>>) do
    <<name::binary-size(len), remains::binary>> = data
    parse_name(remains, raw, name)
  end

  defp parse_name(<<len::8, data::binary>>, raw, acc) do
    <<name::binary-size(len), remains::binary>> = data
    parse_name(remains, raw, acc <> "." <> name)
  end
end
