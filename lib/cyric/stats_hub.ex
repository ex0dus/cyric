defmodule Cyric.StatsHub do
  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def track_connection({action, name}) do
    GenServer.cast(__MODULE__, {:track_connection, {action, name}})
  end

  def track_transfer({action, name, bytes}) do
    GenServer.cast(__MODULE__, {:track_transfer, {action, name, bytes}})
  end

  def fetch_stats do
    GenServer.call(__MODULE__, :fetch_stats)
  end

  @impl true
  def init(_args) do
    :timer.send_interval(1000, self(), :update_speeds)
    {:ok, %{}}
  end

  @impl true
  def handle_call(:fetch_stats, _caller, %{} = state) do
    stats =
      state
      |> Enum.map(fn {name, stats} ->
        %{
          redirector: name,
          connections: Map.get(stats, :connections, 0),
          up: Map.get(stats, :up, 0),
          down: Map.get(stats, :down, 0),
          speed: Map.get(stats, :speed, 0)
        }
      end)

    {:reply, stats, state}
  end

  @impl true
  def handle_cast({:track_connection, {:up, name}}, %{} = state) do
    new_state =
      Map.update(state, name, %{name => %{}}, fn stat ->
        Map.update(stat, :connections, 1, fn c -> c + 1 end)
      end)

    {:noreply, new_state}
  end

  @impl true
  def handle_cast({:track_connection, {:down, name}}, %{} = state) do
    if Map.get(state, name) do
      new_state =
        Map.update!(state, name, fn stat ->
          Map.update(stat, :connections, 0, fn c -> c - 1 end)
        end)

      {:noreply, new_state}
    else
      {:noreply, state}
    end
  end

  @impl true
  def handle_cast({:track_transfer, {action, name, bytes}}, %{} = state) do
    if Map.get(state, name) do
      new_state =
        Map.update!(state, name, fn stats ->
          stats
          |> Map.update(action, 0, fn b -> b + bytes end)
          |> Map.update(:window, 0, fn b -> b + bytes end)
        end)

      {:noreply, new_state}
    else
      {:noreply, state}
    end
  end

  @impl true
  def handle_info(:update_speeds, %{} = state) do
    new_state =
      state
      |> Enum.map(fn {name, stats} ->
        {name, Map.merge(stats, %{speed: Map.get(stats, :window, 0), window: 0})}
      end)
      |> Enum.into(%{})

    {:noreply, new_state}
  end
end
