defmodule Cyric.Vmess.Protocol do
  def gen_time_bin do
    DateTime.utc_now()
    |> DateTime.to_unix()
    # |> Kernel.+(:rand.uniform(60))
    # |> Kernel.-(30)
    |> (&<<&1::64>>).()
  end

  def gen_data_key(key) do
    one = :crypto.hash(:md5, key)
    two = :crypto.hash(:md5, one)
    one <> two
  end

  def uid_bin(uid) do
    uid
    |> String.replace("-", "")
    |> String.upcase()
    |> Base.decode16()
  end

  def empty_payload do
    <<16::16>> <> :binary.copy(<<0>>, 16)
  end

  def gen_nonce_iv(iv, nonce) do
    <<_::binary-size(2), data_iv::binary-size(10), _::binary>> = iv
    <<nonce::16, data_iv::binary>>
  end
end
