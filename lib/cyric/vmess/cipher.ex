defmodule Cyric.Vmess.Cipher do
  use GenServer, restart: :temporary
  use Bitwise, only_operators: true

  alias Cyric.Vmess.Protocol

  require Logger

  @key_aad "c48619fe-8f02-49e0-b9e9-edf763e17e21"
  @ver <<1>>
  @verify <<98>>
  @opts <<1>>
  @keep <<0>>
  @cmd_tcp <<1>>
  # @cmd_udp <<2>>
  @max_len 16383

  defmodule State do
    defstruct target: nil, uid: nil, key: nil, iv: nil, e_nonce: 0, d_nonce: 0, leftover: <<>>
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def encode(pid, data) do
    GenServer.call(pid, {:encode, data})
  end

  def decode(pid, data) do
    GenServer.call(pid, {:decode, data})
  end

  @impl true
  def init({target, uuid}) do
    key = :crypto.strong_rand_bytes(16)
    iv = :crypto.strong_rand_bytes(16)

    {:ok, uid} = Protocol.uid_bin(uuid)
    {:ok, %State{target: target, key: key, iv: iv, uid: uid}}
  end

  @impl true
  def handle_call({:encode, <<>>}, _from, %State{} = state), do: {:reply, <<>>, state}

  @impl true
  def handle_call({:encode, payload}, _from, %State{} = state) do
    %{uid: uid, target: target, key: key, iv: iv, e_nonce: nonce} = state
    {:ok, data, new_nonce} = encode(payload, target, uid, key, iv, nonce)
    {:reply, data, %State{state | e_nonce: new_nonce}}
  end

  @impl true
  def handle_call({:decode, <<>>}, _from, %State{} = state), do: {:reply, <<>>, state}

  @impl true
  def handle_call({:decode, payload}, _from, %State{} = state) do
    %{key: key, iv: iv, d_nonce: nonce, leftover: leftover} = state
    key = :crypto.hash(:md5, key)
    iv = :crypto.hash(:md5, iv)

    {:ok, data, new_nonce, leftover} = decode(leftover <> payload, key, iv, nonce)
    {:reply, data, %State{state | d_nonce: new_nonce, leftover: leftover}}
  end

  defp decode_head(data, key, iv) do
    :crypto.crypto_one_time(:aes_128_cfb128, key, iv, data, false)
  end

  defp decode(payload, key, iv, nonce) do
    data_key = Protocol.gen_data_key(key)

    case nonce do
      0 ->
        <<header::binary-size(4), remains::binary>> = payload
        <<_v, _opt, _cmd, m>> = decode_head(header, key, iv)
        <<_cmd_data::binary-size(m), data::binary>> = remains
        decode_content(data, data_key, iv, 0, <<>>)

      _ ->
        decode_content(payload, data_key, iv, nonce, <<>>)
    end
  end

  defp decode_content(<<len::16, data::binary>> = payload, _key, _iv, nonce, acc)
       when len > byte_size(data),
       do: {:ok, acc, nonce, payload}

  defp decode_content(<<len::16, payload::binary>>, key, iv, nonce, acc) do
    binlen = len - 16
    <<enc::binary-size(binlen), tag::binary-size(16), remains::binary>> = payload
    nonce_iv = Protocol.gen_nonce_iv(iv, nonce)

    data = :crypto.crypto_one_time_aead(:chacha20_poly1305, key, nonce_iv, enc, <<>>, tag, false)

    decode_content(remains, key, iv, nonce + 1, acc <> data)
  end

  defp decode_content(payload, _key, _iv, nonce, acc), do: {:ok, acc, nonce, payload}

  defp encode(payload, target, uid, key, iv, nonce) do
    case nonce do
      0 ->
        time_bin = Protocol.gen_time_bin()
        auth = :crypto.mac(:hmac, :md5, uid, time_bin)
        inst = build_inst(target, uid, time_bin, key, iv)
        {:ok, data, new_nonce} = encode_data(payload, key, iv, 0, <<>>)
        {:ok, auth <> inst <> data, new_nonce}

      _ ->
        encode_data(payload, key, iv, nonce, <<>>)
    end
  end

  defp encode_data(payload, key, iv, nonce, acc) do
    data_key = Protocol.gen_data_key(key)
    nonce_iv = Protocol.gen_nonce_iv(iv, nonce)
    len = byte_size(payload)

    if len > @max_len do
      binlen = @max_len + 16
      <<payload::binary-size(@max_len), remains::binary>> = payload

      {enc, tag} =
        :crypto.crypto_one_time_aead(:chacha20_poly1305, data_key, nonce_iv, payload, <<>>, true)

      encode_data(remains, key, iv, nonce + 1, acc <> <<binlen::16>> <> enc <> tag)
    else
      binlen = len + 16

      {enc, tag} =
        :crypto.crypto_one_time_aead(:chacha20_poly1305, data_key, nonce_iv, payload, <<>>, true)

      {:ok, acc <> <<binlen::16>> <> enc <> tag, nonce + 1}
    end
  end

  defp build_inst(address, uid, time_bin, enc_key, enc_iv) do
    key = :crypto.hash(:md5, uid <> @key_aad)
    iv = :crypto.hash(:md5, :binary.copy(time_bin, 4))

    pad_len = :rand.uniform(15)
    padding = :crypto.strong_rand_bytes(pad_len)

    addrs = Cyric.Address.to_vmess_binary(address)

    cmd = <<pad_len::4, 4::4>>

    inst_data =
      @ver <>
        enc_iv <> enc_key <> @verify <> @opts <> cmd <> @keep <> @cmd_tcp <> addrs <> padding

    inst_crc = <<Fnv1a.hash(inst_data)::32>>
    :crypto.crypto_one_time(:aes_128_cfb128, key, iv, inst_data <> inst_crc, true)
  end
end
