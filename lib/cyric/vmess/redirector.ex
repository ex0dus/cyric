defmodule Cyric.Vmess.Redirector do
  use Cyric.Redirector

  alias Cyric.Address
  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def handle_init(_conf) do
    {:ok, %State{}}
  end

  @impl true
  def handle_connect(target, %State{conf: conf} = state) do
    %{host: host, port: port, uuid: uuid} = conf

    {:ok, ts} =
      Cyric.Tcp.StreamSup.start_stream({:outgoing, Address.new(:domain, host, port), false})

    codec = Cyric.Vmess.Codec.new(target, uuid)
    stream_out = Cyric.EncodedStream.new(ts, codec)
    {:ok, %{state | out: stream_out}}
  end
end
