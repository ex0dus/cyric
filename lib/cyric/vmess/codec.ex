defmodule Cyric.Vmess.Codec do
  defstruct cipher: nil

  alias Cyric.Vmess.Codec

  def new(target, uuid) do
    {:ok, cipher} = Cyric.Vmess.Cipher.start_link({target, uuid})
    %Codec{cipher: cipher}
  end

  def encode(%Codec{cipher: cipher}, raw) do
    GenServer.call(cipher, {:encode, raw})
  end

  def decode(%Codec{cipher: cipher}, encoded) do
    GenServer.call(cipher, {:decode, encoded})
  end
end

defimpl Cyric.Codec, for: Cyric.Vmess.Codec do
  alias Cyric.Vmess.Codec

  def encode(codec, raw) do
    Codec.encode(codec, raw)
  end

  def decode(codec, encoded) do
    Codec.decode(codec, encoded)
  end
end
