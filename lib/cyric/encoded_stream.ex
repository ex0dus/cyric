defmodule Cyric.EncodedStream do
  defstruct tcp_stream: nil, codec: nil

  def new(ts, codec) do
    %__MODULE__{tcp_stream: ts, codec: codec}
  end
end

defimpl Cyric.Streamable, for: Cyric.EncodedStream do
  alias Cyric.Codec
  alias Cyric.Tcp.Stream
  alias Cyric.EncodedStream

  def read(%EncodedStream{tcp_stream: stream, codec: codec}, len) do
    case Stream.read(stream, len) do
      {:ok, <<>>} ->
        {:ok, <<>>}

      {:ok, payload} ->
        {:ok, Codec.decode(codec, payload)}

      {:error, reason} ->
        {:error, reason}
    end
  end

  def write(_stream, <<>>), do: :ok

  def write(%EncodedStream{tcp_stream: stream, codec: codec}, payload) do
    data = Codec.encode(codec, payload)
    Stream.write(stream, data)
  end

  def set_active(%EncodedStream{tcp_stream: stream}, active) do
    Stream.set_active(stream, active)
  end

  def close(%EncodedStream{tcp_stream: stream}) do
    Stream.close(stream)
  end
end
