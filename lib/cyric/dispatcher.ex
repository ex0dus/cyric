defmodule Cyric.Dispatcher do
  use GenServer
  alias Cyric.Rule.{Parser, Matcher}

  defmodule State do
    defstruct proxies: nil, rules: nil
  end

  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def redirect({target, stream_in}) do
    GenServer.cast(__MODULE__, {:redirect, {target, stream_in}})
  end

  def fetch_proxies do
    GenServer.call(__MODULE__, :fetch_proxies)
  end

  def fetch_rules do
    GenServer.call(__MODULE__, :fetch_rules)
  end

  @impl true
  def init(config) do
    {:ok, nil, {:continue, {:load, config}}}
  end

  @impl true
  def handle_continue({:load, [proxy_conf, rule_conf]}, _state) do
    proxies = Parser.parse_proxies(proxy_conf)
    Logger.debug("load proxies #{inspect(proxies)}")
    rules = Parser.parse_rules(rule_conf)
    Logger.debug("load rules #{inspect(rules)}")
    {:noreply, %State{proxies: proxies, rules: rules}}
  end

  @impl true
  def handle_call(:fetch_proxies, _caller, %State{proxies: proxies} = state) do
    {:reply, proxies, state}
  end

  @impl true
  def handle_call(:fetch_rules, _caller, %State{rules: rules} = state) do
    {:reply, rules, state}
  end

  @impl true
  def handle_cast({:redirect, {target, stream_in}}, %State{} = state) do
    %{rules: rules, proxies: proxies} = state

    Task.start(fn ->
      match_redirect(target, stream_in, rules, proxies)
    end)

    {:noreply, state}
  end

  defp match_redirect(target, stream_in, rules, proxies) do
    proxy =
      case Matcher.match(rules, target) do
        nil -> Map.get(rules, :match)
        :not_found -> Map.get(rules, :match)
        {:ok, proxy} -> proxy
      end

    {name, redirector} =
      Enum.find(proxies, fn {name, _} ->
        name == proxy
      end) || {:direct, {Cyric.Tcp.Redirector, %{name: "DIRECT"}}}

    Logger.debug("redirect #{inspect(target)} using #{name}")
    Cyric.RedirectorSup.start_redirect(redirector, target, stream_in)
  end
end
