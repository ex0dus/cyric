defmodule Cyric.Tcp.Stream do
  use GenServer, restart: :temporary

  @socket_opts [:binary, packet: :raw, reuseaddr: true]

  alias Cyric.Address

  defmodule State do
    defstruct socket: nil, active: false, buf: <<>>, stopped: nil

    @type t :: %__MODULE__{
            socket: port,
            active: boolean,
            buf: binary,
            stopped: String.t() | nil
          }
  end

  def start(args) do
    GenServer.start(__MODULE__, args)
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def read(pid, len \\ 0, timeout \\ :infinity) do
    GenServer.call(pid, {:read, len, timeout})
  end

  def write(pid, data) do
    GenServer.cast(pid, {:write, data})
  end

  def set_active(pid, active) do
    GenServer.call(pid, {:set_active, active})
  end

  def close(pid) do
    GenServer.stop(pid, :normal)
  end

  @impl true
  def init({:outgoing, address, active}) do
    opts = [{:active, active} | @socket_opts]

    {:ok, addr, port} = Address.to_inet(address)
    {:ok, socket} = :gen_tcp.connect(addr, port, opts)
    {:ok, %State{active: active, socket: socket}}
  end

  @impl true
  def init({:incoming, socket, active}) do
    {:ok, %State{active: active, socket: socket}}
  end

  # @impl true
  # def terminate(_reason, %{socket: socket}) do
  #   :gen_tcp.close(socket)
  # end

  @impl true
  def handle_call({:set_active, active}, _from, %State{socket: socket} = state) do
    :ok = :inet.setopts(socket, active: active)
    {:reply, :ok, %{state | active: active}}
  end

  @impl true
  def handle_call({:read, len, _timeout}, _from, %State{active: false, socket: socket} = state) do
    case :gen_tcp.recv(socket, len) do
      {:ok, packet} ->
        {:reply, {:ok, packet}, state}

      {:error, reason} ->
        {:reply, {:shutdown, reason}, state}
    end
  end

  @impl true
  def handle_call({:read, _len, _timeout}, _from, %State{active: true, buf: buf} = state) do
    case state do
      %{stopped: nil} ->
        {:reply, {:ok, buf}, %{state | buf: <<>>}}

      %{stopped: reason} ->
        {:reply, {:error, reason}, state}
    end
  end

  @impl true
  def handle_cast({:write, data}, %State{socket: socket} = state) do
    case :gen_tcp.send(socket, data) do
      :ok ->
        {:noreply, state}

      {:error, reason} ->
        {:noreply, %{state | stopped: reason}}
    end
  end

  @impl true
  def handle_info({:tcp, _socket, data}, %State{buf: buf} = state) do
    {:noreply, %{state | buf: buf <> data}}
  end

  @impl true
  def handle_info({:tcp_closed, _socket}, state) do
    {:noreply, %{state | stopped: "closed"}}
  end

  @impl true
  def handle_info({:tcp_error, _socket, reason}, state) do
    {:noreply, %{state | stopped: reason}}
  end

  defp getaddr({:domain, host, port}) do
    case :inet.getaddr(to_charlist(host), :inet) do
      {:ok, addr} ->
        {:ok, addr, port}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp getaddr({:ipv4, host, port}) do
    {:ok, host, port}
  end
end
