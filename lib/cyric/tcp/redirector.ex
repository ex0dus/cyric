defmodule Cyric.Tcp.Redirector do
  use Cyric.Redirector

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def handle_init(_conf) do
    {:ok, %State{}}
  end

  @impl true
  def handle_connect(address, state) do
    with {:ok, ts} <- Cyric.Tcp.StreamSup.start_stream({:outgoing, address, false}) do
      {:ok, %{state | out: %Cyric.RawStream{tcp_stream: ts}}}
    else
      reason ->
        {:error, reason}
    end
  end
end
