defmodule Cyric.Tcp.Listener do
  use GenServer
  require Logger

  alias Cyric.Streamable

  @socket_opts [:binary, packet: :raw, active: false, reuseaddr: true]

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def init(%{port: port}) do
    {:ok, socket} = :gen_tcp.listen(port, @socket_opts)
    Logger.info("listening tcp on #{port}")
    {:ok, socket, {:continue, :accept}}
  end

  def handle_continue(:accept, socket) do
    {:ok, client} = :gen_tcp.accept(socket)

    {:ok, task} = Task.start(fn -> dispatch(client) end)
    :gen_tcp.controlling_process(client, task)

    {:noreply, socket, {:continue, :accept}}
  end

  defp dispatch(socket) do
    {:ok, tcp_stream} = Cyric.Tcp.StreamSup.start_stream({:incoming, socket, false})
    :gen_tcp.controlling_process(socket, tcp_stream)
    stream = %Cyric.RawStream{tcp_stream: tcp_stream}

    case :inet.getopts(socket, [{:raw, 0, 80, 16}]) do
      {:ok, [{:raw, 0, 80, <<_::16, port::16, a, b, c, d, _::binary>>}]} ->
        target = Cyric.Address.new(:ipv4, {a, b, c, d}, port)
        Cyric.Dispatcher.redirect({target, stream})

      {:error, reason} ->
        Logger.info("no address found, #{reason}, ignoring")
    end
  end
end
